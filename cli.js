#!/usr/bin/env node
const argv = require('yargs').argv
const fsx = require('fs-extra')
const { join, resolve } = require('path')
const { chainPromises, baseConfig } = require('./src/helpers')
const { spawn } = require('child_process')

console.info(argv)

const env = process.env.NODE_ENV;
// paths
const baseDir = process.cwd()
const srcDir = join(__dirname, 'src')
// first thing we need to to is actually read the package.json
// if it doesn't exist then exit!
const packageJsonFile = join(baseDir, 'package.json')
const pkg = fsx.readJsonSync(packageJsonFile)
if (!pkg) {
  console.error('Sorry you got to init project before you can use this')
  process.exit()
}

// npx create-qunit or npm init qunit
// or call this cli interface it will run a setup

let userBaseDir;
if (argv.baseDir && fsx.existsSync(resolve(argv.baseDir))) {
  userBaseDir = resolve(argv.baseDir)

  console.info('Your base directory will be', userBaseDir)
} else { // v1.1.0
  let testsDir = join(baseDir, 'tests')
  if (fsx.existsSync(testsDir)) {
    // check if there is a tests folder
    userBaseDir = testsDir;
  }
}
// we only interested with create-qunit ./dist,./other_folder
let dirs = argv._;
if (dirs.length) {
  dirs = dirs
    .map(dir => resolve(dir))
    .filter(dir => fsx.existsSync(dir))

  console.info('additional directory will add to webroot', dirs)
}
// now add with ours
const qunitDir = userBaseDir ? join(userBaseDir, 'qunit') : join(baseDir, 'qunit')
let files = [
  join('files', 'hello-world-test.js'),
  join('webroot', 'index.html'),
  join('webroot', 'nb-qunit-helper.js'),
  join('run-qunit-setup.js')
]
chainPromises(files
  .map(file => fsx.copy(
      join(srcDir, 'fixtures', file),
      join(qunitDir, file)
    )
  ))
  .catch(error => {
    console.error(`Sorry we encounter an error and have to quit`, error)
    process.exit()
  })
  .then(results => {
    console.info('Files copy successfully, now creating config file')

    // continue with the generate config file
    baseConfig.baseDir = userBaseDir || baseDir;
    baseConfig.webroot = Array.isArray(dirs) && dirs.length ? baseConfig.webroot.concat(dirs) : baseConfig.webroot;
    // need to fix the webroot directory here
    baseConfig.webroot = baseConfig.webroot
      .map( dir => resolve(dir) )
      .map( dir => dir.replace(baseDir, '.'))

    // generate the file
    const content = `
const runQunitSetup = require('./run-qunit-setup')
const config = ${JSON.stringify(baseConfig, null, 2)}

runQunitSetup(config)
`;
    fsx.outputFile(join(baseConfig.baseDir, 'qunit', 'run-qunit.js'), content, err => {
      if (err) {
        console.error(`We couldn't generate the run file. Exit now`, err)
        process.exit()
        return
      }
      const qunitScript = 'qunit/run-qunit.js';
      let scriptPath = userBaseDir ? join(userBaseDir, qunitScript) : './' + qunitScript;
      // now add stuff to the package.json
      pkg.scripts['qunit'] = `node ${scriptPath.replace(baseDir + '/', './')}`
      // v1.1.0
      // check to see if there is an AVA setup
      if (pkg.ava) {
        pkg.ava.files = pkg.ava.files || []
      }
      let ignorePath = userBaseDir ? join(userBaseDir,'qunit') : './qunit';
      pkg.ava.files.push(`!${ignorePath.replace(baseDir + '/', '')}/*.*`)

      fsx.writeJson(packageJsonFile, pkg, {spaces: 2}, function(err) {
        if (err) {
          console.error(`Sorry we couldn't write the config to package.json`, err)
          return
        }
        console.info('Your package.json has been updated')
        // add dependencies
        if (process.env.NODE_ENV !== 'test' && !argv.skipInstall) {
          console.info('Running install dependencies ...')
          // install dependencies
          const es = spawn('npm', [
            'install',
            'server-io-core',
            '--save-dev'
          ])
          es.on('close', code => {
            console.info(`It seems we are done, now you can run "npm run qunit"`)
          })
        } else {
          if (argv.skipInstall) {
            console.info('If you don\'t have server-io-core install then run "npm install server-io-core --save-dev"')
          } else {
            console.info('This is all done, exit now')
          }
        }
      })
    })
  })
