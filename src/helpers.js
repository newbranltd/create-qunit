// our cool little tool
const { join } = require('path')

function chainPromises(promises) {
  return promises.reduce((promiseChain, currentTask) => (
    promiseChain.then(chainResults => (
      currentTask.then(currentResult => (
        [...chainResults, currentResult]
      ))
    ))
  ), Promise.resolve([]))
}

const baseConfig = {
  port: 8081,
  webroot: [
    'qunit/webroot',
    'qunit/files'
  ],
  open: true,
  reload: true,
  testFilePattern: '*-test.js'
}

// export
module.exports = {
  chainPromises,
  baseConfig
}
