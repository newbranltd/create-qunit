const test = require('ava')
const { join, resolve } = require('path')
const fsx = require('fs-extra')
const { spawn } = require('child_process')

const debug = require('debug')('create-qunit:test:basic')

const baseDir = join(__dirname, 'tmp')

test.before( t => {
  fsx.ensureDir(join(__dirname, 'tmp'), err => {
    if (err) {
      console.error(err)
      return
    }
    fsx.copy(
      join(__dirname, '..', 'package.json'),
      join(__dirname, 'tmp', 'package.json')
    )
  })
})

test.after( t => {
  fsx.removeSync(join(__dirname, 'tmp'))
})

test.serial.cb('It should able to run and copy fixtures files to the dest', t => {
  t.plan(1)

  const es = spawn('node',
    [resolve(join(__dirname, '..', 'cli.js'))],
    {cwd: resolve((join(__dirname, 'tmp')))}
  )

  es.stdout.on('data', data => {
    debug('stdout', `${data}`)
  })
  es.stderr.on('data', data => {
    debug('stderr', `${data}`)
  })

  es.on('close', code => {
    debug('close with code', code)
    t.truthy(fsx.existsSync(join(baseDir, 'qunit', 'run-qunit.js')))
    t.end()
  })
})

test.serial.cb('It should able to accept the baseDir parameter', t => {
  t.plan(2)
  // this will also test the v1.1.0 feature to search for tests folder
  fsx.ensureDir(join(__dirname, 'tmp', 'tests'))

  const es = spawn('node',
    [
      resolve(join(__dirname, '..', 'cli.js'))
      // '--baseDir',
      // './tests'
    ],
    {cwd: resolve((join(__dirname, 'tmp')))}
  )

  es.stdout.on('data', data => {
    debug('stdout', `${data}`)
  })
  es.stderr.on('data', data => {
    debug('stderr', `${data}`)
  })

  es.on('close', code => {
    debug('close with code', code)
    t.truthy(fsx.existsSync(join(baseDir, 'tests', 'qunit', 'run-qunit.js')))
    // also test the v1.1.0 insert extra ignore into the ava part in package.json
    let pkg = fsx.readJsonSync(join(baseDir, 'package.json'))
    // because it was added one before
    t.is(pkg.ava.files.length, 4)

    t.end()
  })
})

test.serial.cb('It should able to add additonal paths to the webroot', t => {
  t.plan(1)
  // remove it then create it again
  fsx.removeSync(join(__dirname, 'tmp', 'tests'))
  fsx.ensureDir(join(__dirname, 'tmp', '__tests__'))
  // run it
  const es = spawn('node',
    [
      resolve(join(__dirname, '..', 'cli.js')),
      './dist',
      './node_modules',
      '--baseDir',
      './__tests__'
    ],
    {cwd: resolve((join(__dirname, 'tmp')))}
  )

  es.stdout.on('data', data => {
    debug('stdout', `${data}`)
  })
  es.stderr.on('data', data => {
    debug('stderr', `${data}`)
  })

  es.on('close', code => {
    debug('close with code', code)
    t.truthy(fsx.existsSync(join(__dirname, 'tmp', '__tests__', 'qunit', 'run-qunit.js')))
    t.end()
  })
})
